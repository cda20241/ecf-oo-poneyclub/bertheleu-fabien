package org.example;

class Salarie extends Personne {
    private String employeur;
    private String fonction;
    //      CONSTRUCTEUR        //
    public Salarie(String nom, String prenom, String employeur, String fonction) {
        super(nom, prenom);
        this.employeur = employeur;
        this.fonction = fonction;
    }
    //      GETTERS      //
    public String getEmployeur() {
        return employeur;
    }

    public String getFonction() {
        return fonction;
    }
    //      SETTERS      //
    public void setEmployeur(String employeur) {
        this.employeur = employeur;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }
}