package org.example;

public class Criniere {
    private Integer longueurCriniere;

    private textureCriniere textureCriniere;

    ////      CONSTRUCTEURS       ////
    public Criniere(Integer longueurCriniere, org.example.textureCriniere textureCriniere) {
        this.longueurCriniere = longueurCriniere;
        this.textureCriniere = textureCriniere;
    }
    ////        GETTERS          ////
    public Integer getLongueurCriniere() {
        return longueurCriniere;
    }

    public org.example.textureCriniere getTextureCriniere() {
        return textureCriniere;
    }
    ////        SETTERS     ////
    public void setLongueurCriniere(Integer longueurCriniere) {
        this.longueurCriniere = longueurCriniere;
    }

    public void setTextureCriniere(org.example.textureCriniere textureCriniere) {
        this.textureCriniere = textureCriniere;
    }
}
