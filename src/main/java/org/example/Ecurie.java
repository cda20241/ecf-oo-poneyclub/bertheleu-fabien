package org.example;

import java.util.ArrayList;
import java.util.List;

class Ecurie {
    public Object getPoneys;
    private String nomEcurie;
    private List<Salarie> employesDuClub;
    private List<Box> boxes;
    private List<Poney> poneys;
       private int FoinTotal;
    private int foinApresLivraison;
    private int capaciteTotaledeFoin;
    private float repartitionIdeale;

    ////      CONSTRUCTEUR        ////
    public Ecurie() {
        this.employesDuClub = new ArrayList<>();
        this.boxes = new ArrayList<>();
        this.poneys = new ArrayList<>();
    }

    ////      GETTER      ////
    public List<Salarie> getEmployesDuClub() {
        return employesDuClub;
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public List<Poney> getPoneys() {
        return poneys;
    }

    public int getFoinTotal() {
        return FoinTotal;
    }

    public int getFoinApresLivraison() {
        return foinApresLivraison;
    }

    public String getNomEcurie() {
        return nomEcurie;
    }

    public float getRepartitionIdeale() {
        return repartitionIdeale;
    }

    public int getCapaciteTotaledeFoin() {
        return capaciteTotaledeFoin;
    }

    ////        SETTERS         ////

    public void setFoinTotal(int foinTotal) {
        FoinTotal = foinTotal;
    }

    public void setEmployesDuClub(List<Salarie> employesDuClub) {
        this.employesDuClub = employesDuClub;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public void setPoneys(List<Poney> poneys) {
        this.poneys = poneys;
    }

    public void setFoinApresLivraison(int foinApresLivraison) {
        this.foinApresLivraison = foinApresLivraison;
    }

    public void setCapaciteTotaledeFoin(int capaciteTotaledeFoin) {
        this.capaciteTotaledeFoin = capaciteTotaledeFoin;
    }

    public void setNomEcurie(String nomEcurie) {
        this.nomEcurie = nomEcurie;
    }

    public void setRepartitionIdeale(float repartitionIdeale) {
        this.repartitionIdeale = repartitionIdeale;
    }

    ////        METHODES        ////

    public Box ajouterBox(String nomDuBox, Integer taille, Integer quantiteFoinCourante, Integer quantiteFoinMax) {
        Box box = new Box(nomDuBox,taille,quantiteFoinCourante,quantiteFoinMax);
        boxes.add(box);
        return box;
    }



    /** Méthode pour employer un salarié en l'ajoutant à la liste des employés du club
     *
     * @param salarie
     */
    public void employer(Salarie salarie) {
        employesDuClub.add(salarie);
        System.out.println("Nouvel employé ajouté : " + salarie.getNom() + " " + salarie.getPrenom() +
                ", Fonction : " + salarie.getFonction() + ", Employeur : " + salarie.getEmployeur());
    }

    /**Méthode qui licencie un employé en l'enlevant de la liste des salariés
     *
     * @param salarie
     */
    public void licencier(Salarie salarie) {
        if (employesDuClub.contains(salarie)) {
            employesDuClub.remove(salarie);
            System.out.println("Employé licencié : " + salarie.getNom() + " " + salarie.getPrenom());
        } else {
            System.out.println("Cet employé n'est pas dans la liste des employés.");
        }
    }

    /**Méthode pour acheter, acquérir un ¨Poney, et qui ajoute le Poney à la liste des Poneys du club
     *
     * @param nomPoney
     * @param poids
     * @param longueurCriniere
     * @param textureCriniere
     */
    public Poney acquerirPoney(String nomPoney, int poids, int longueurCriniere, textureCriniere textureCriniere) {
        Poney nouveauPoney = new Poney(nomPoney, poids, longueurCriniere, textureCriniere); // Crée un nouveau Poney
        poneys.add(nouveauPoney); // Ajoute le Poney à la liste des poneys de l'écurie
        System.out.println("Vous venez d'acquérir un nouveau poney");
        return nouveauPoney;
    }

    /** Cette méthode est créée pour afficher la liste des Poneys du Club.
     *
     */
        public void imprimerListePoneys() {
        System.out.println("Liste des poneys de l'écurie :");
        for (Poney poney : poneys) {
            System.out.println("Nom : " + poney.getNomPoney());
                    }
    }

    /**Methode qui rentre un poney dans un box, en vérifiant si le Poney n'est pas déjà dans un box
     *
     * @param poney
     * @param box
     */
    public void rentrerPoney(Poney poney, Box box) {
        // Cette boucle pour vérifier si le Poney n'est pas déjà dans un autre Box, sinon, on sort de la boucle
        // et on passe à la suite
        for (Box autreBox : boxes) {
            if (autreBox.getPoneyDuBox().contains(poney.getNomPoney())) {
                System.out.println("Le poney " + poney.getNomPoney() + " est déjà dans le box " + autreBox.getNomDuBox());
                return; // Sortir de la méthode si le poney est déjà présent dans un box
            }
        }
        // je vérifie s'il y a assez de place dans le box pour le poney
        if (box.getTaille() <= box.getPoneyDuBox().size()) {
            System.out.println("Il n'y a pas assez de place dans le box " + box.getNomDuBox() + " pour le poney " + poney.getNomPoney());
        } else {
            // Mes deux conditions sont vérifiées, j'ajoute le poney au Box
            poneys.add(poney);
            box.ajouterPoneyAuBox(poney);
            System.out.println("Le poney " + poney.getNomPoney() + " a été placé dans le box " + box.getNomDuBox());
        }
    }

    /**je créé une méthode envoyerPaitre dans laquelle je cherche le poney selon son nom dans les box et l'en enlève.
     * Si jamais je ne trouve pas son nom, je précise qu'il n'est pas dans les box
     * @param poney
     */
    public void envoyerPaitre(Poney poney) {
        for (Box box : boxes) {
            if (box.getPoneyDuBox().contains(poney)) {
                box.supprimerPoneyDuBox(poney);
                poneys.remove(poney);
                System.out.println("Le Poney " + poney.getNomPoney() + " a bien été aux champs pour paître de la bonne herbe fraîche !");
                return; // je sors de la boucle si le poney a été trouvé et retiré
            }
        }
        System.out.println("Le poney n'a pas été trouvé dans les boxes de l'écurie.");
    }

    /**Methode qui permet de faire une recherche d'un Box par son nom de Box
     *
     * @param nomBox
     * @return
     */
    public Box trouverBoxParNom(String nomBox) {
        for (Box box : boxes) {
            if (box.getNomDuBox().equals(nomBox)) {
                return box; // Retourne le box si le nom correspond
            }
        }
        return null; // Retourne null si aucun box n'est trouvé avec le nom spécifié

//    public void creerLivraison(Livraison livraison, String numeroLot, Integer quantiteLot, TypeFoin typeFoin){
//        Foin foin = new Foin (numeroLot, quantiteLot, typeFoin);
//        livraison.setLotDeFoin(foin);
    }

    /**Methode qui permet d'afficher, concernant le foin, le pourcentage de remplissage des box
     *
     */
    public void affichageRemplissageDesBox() {
        for (Box box : getBoxes()) {
            float tauxRemplissage = (float) box.getQuantiteFoinCourante() / box.getQuantiteFoinMax() * 100;
            System.out.println("Taux Remplissage du Box " +box.getNomDuBox()+ " : " + tauxRemplissage + "%");
        }
    }

    /**Methode qui permet de calculer le stock de foin dans l'écurie, avant livraison.
     *
     */
    public void foinTotal() {
        int foinTotal = 0;
        for (Box box : getBoxes()) {
            foinTotal = box.getQuantiteFoinCourante() + foinTotal;
        }
        setFoinTotal(foinTotal);
    }

    /**Méthode qui permet de quantifier le stock maximal de l'écurie dans son ensemble.
     *
     */
     public void CapaciteTotaleDeFoin(){
        int CapaciteTotale =0 ;
        for (Box box : getBoxes()) {
            CapaciteTotale = box.getQuantiteFoinMax() + CapaciteTotale;
        }
        setCapaciteTotaledeFoin(CapaciteTotale);
          }

    /**Methode qui permet de calculer le foin qu'il faudrait donner dans chaque box pour qu'ils aient tous la
     * même quantité de foin à l'intérieur
     * @param livraison
     */
    public void calculRepartitiondesBoxenkg(Livraison livraison){
        int foinApresLivraison = getFoinTotal()+livraison.getLotDeFoin().getQuantiteLot();
        setFoinApresLivraison(foinApresLivraison);
        int repartitionEquitable = foinApresLivraison / getBoxes().size();
        System.out.println("Pour répartir équitablement le foin dans les box, il faudrait mettre " + repartitionEquitable+"kg dans chaque Box");
    }

    /**Methode qui permet de calculer la proportion  idéale de foin dans chaque box après la dernière livraison
     *
     */
    public void calculPartduFoinTotal(){
        CapaciteTotaleDeFoin();
        float repartitionIdeale = ((float) getFoinApresLivraison() /getCapaciteTotaledeFoin()*100);
        System.out.println("Actuellement, l'Ecurie est remplie à " + repartitionIdeale +"% de sa capacité totale de foin qui est de " +getCapaciteTotaledeFoin());
        setRepartitionIdeale(repartitionIdeale);

    }

    /**Methode qui vide les Box de tout leur foin
     *
     */
    public void viderLesBox(){
        for (Box box : getBoxes()) {
            box.setQuantiteFoinCourante(0);
            }
    }

    /**Methode qui remplit les box avec une proportion équitable de foin en prenant en compte le foin de l'écurie
     * après livraison, la capacité maximum de chaque box, afin qu'ils aient tous le même taux de remplissage
     */
    public void remplirLesBox(){
        for (Box box : getBoxes()) {
            float variable = (box.getQuantiteFoinMax()*getRepartitionIdeale()/100);
            box.setQuantiteFoinCourante((int) variable);
            System.out.println("La nouvelle Quantité de foin du Box " + box.getNomDuBox() + " est de " +box.getQuantiteFoinCourante());
        }
    }
}




