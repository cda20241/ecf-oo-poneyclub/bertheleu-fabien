package org.example;

public class Poney {
    private String nomPoney;
    private Integer poidsPoney;
    private Integer longueurCriniere;

    private textureCriniere criniere;


    //      CONSTRUCTEUR        //
    public Poney(String nomPoney, Integer poidsPoney, Integer longueurCriniere, textureCriniere criniere) {
        this.nomPoney = nomPoney;
        this.poidsPoney = poidsPoney;
        this.longueurCriniere = longueurCriniere;
        this.criniere = criniere;
    }
    //      GETTER      //
    public String getNomPoney() {
        return nomPoney;
    }

    public Integer getPoidsPoney() {
        return poidsPoney;
    }

    public Integer getLongueurCriniere() {
        return longueurCriniere;
    }

    public textureCriniere getCriniere() {
        return criniere;
    }


    //      SETTER      //

    public void setNomPoney(String nomPoney) {
        this.nomPoney = nomPoney;
    }

    public void setPoidsPoney(Integer poidsPoney) {
        this.poidsPoney = poidsPoney;
    }

    public void setLongueurCriniere(Integer longueurCriniere) {
        this.longueurCriniere = longueurCriniere;
    }

    public void setCriniere(textureCriniere criniere) {
        this.criniere = criniere;
    }

}


