package org.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    private List<Livraison> livraisons = new ArrayList<>();
    ////        GETTER          ////
    public List<Livraison> getLivraisons() {
        return livraisons;
    }
    ////        SETTER      ////
    public void setLivraisons(List<Livraison> livraisons) {
        this.livraisons = livraisons;
    }

    public static void main(String[] args) {
        Ecurie ecurie = new Ecurie();
        // Création des boxes
        Box boxA = ecurie.ajouterBox("A", 3, 200, 600);
        Box boxB = ecurie.ajouterBox("B", 4, 300, 600);
        Box boxC = ecurie.ajouterBox("C", 5, 900, 1000);
        Box boxD = ecurie.ajouterBox("D", 6, 100, 800);

        ecurie.setNomEcurie("Tatooine Poney Club");
        System.out.println("______________________________________________");
        System.out.println("     Bienvenue au " + ecurie.getNomEcurie());
        System.out.println("______________________________________________");
        System.out.println("           Scénario : Employés");
        System.out.println("______________________________________________");
        // Création de Luc et Obiwan pour exercice
        Salarie employe1 = new Salarie("KENOBI", "Obiwan", "Tatooine Poney Club", "Maître");
        Salarie employe2 = new Salarie("SKYWALKER", "Luc", "Tatooine Poney Club", "Padawan");

        // Ajout des employés à l'écurie en utilisant la méthode employer()
        ecurie.employer(employe1);
        ecurie.employer(employe2);

        // Affichage de la liste des employés
        System.out.println("---------- Liste des employés de l'écurie :");
        for (Salarie salarie : ecurie.getEmployesDuClub()) {
            System.out.println(salarie.getNom() + " " + salarie.getPrenom() +
                    ", Fonction : " + salarie.getFonction() + ", Employeur : " + salarie.getEmployeur());
        }

        // Essai de Licenciement d'un employé
        ecurie.licencier(employe1);

        // Affichage mis à jour de la liste des employés
        System.out.println("---------- Liste des employés de l'écurie après un test de licenciement :");
        for (Salarie salarie : ecurie.getEmployesDuClub()) {
            System.out.println(salarie.getNom() + " " + salarie.getPrenom() +
                    ", Fonction : " + salarie.getFonction() + ", Employeur : " + salarie.getEmployeur());
        }
        System.out.println("______________________________________________");
        System.out.println("           Scénario : Poneys");
        System.out.println("______________________________________________");

        Poney uSainBolt = ecurie.acquerirPoney("Usain Bolt", 250, 25, textureCriniere.LISSE);
        Poney petitTonnerre =  ecurie.acquerirPoney("Petit Tonnerre", 280, 28, textureCriniere.FRISEE);
        ecurie.imprimerListePoneys();
        ecurie.rentrerPoney(uSainBolt, boxA);
        ecurie.rentrerPoney(petitTonnerre, boxB);

            System.out.println("______________________________________________");
            System.out.println("           Scénario : Livraison");
            System.out.println("______________________________________________");

            // Pour l'exercice, création de la livraison F003 réceptionnée par Luc
            Livraison livraison1 = new Livraison("F003", 200, TypeFoin.PRAIRIE);
            livraison1.receptionnerFoin(employe2);

            // Liste des livraisons
            List<Livraison> livraisons = new ArrayList<>();
            livraisons.add(livraison1);

            // Affichage de la liste des livraisons
            System.out.println("Liste des livraisons :");
            for (
                    Livraison livraison : livraisons) {
                System.out.println("Numéro de Lot : " + livraison.lotDeFoin.getNumeroLot() +
                        ", Date de Livraison : " + livraison.date +
                        ", Salarié de Réception : " + livraison.salarie);
            }

            // ESSAI de l'utilisation de la méthode statique pour obtenir la date de livraison d'un lot
            String numeroLotRecherche = "0012"; // je cherche le lot 0012
            Date dateLivraisonLot = Livraison.dateLivraisonDuLot(livraisons, numeroLotRecherche);
            if (dateLivraisonLot != null) {
                System.out.println("Date de livraison du lot " + numeroLotRecherche + " : " + dateLivraisonLot);
            } else {
                System.out.println("Le lot " + numeroLotRecherche + " n'a pas été trouvé.");
            }



            // Ajout des boxes à l'écurie et impression de la liste obtenue ainsi
//            ecurie.ajouterBox(boxA);
//            ecurie.ajouterBox(boxB);
//            ecurie.ajouterBox(boxC);
//            ecurie.ajouterBox(boxD);

            // Affichage de l'état initial des boxes
            System.out.println("______________________________________________");
            System.out.println("Taux de Remplissage équitable avant Livraison");
            System.out.println("______________________________________________");
            ecurie.affichageRemplissageDesBox();
            Livraison livraison = new Livraison("F003", 200, TypeFoin.PRAIRIE);
            System.out.println("______________________________________________");
            System.out.println("Taux de Remplissage équitable après Livraison");
            System.out.println("______________________________________________");
            ecurie.foinTotal();
            ecurie.calculRepartitiondesBoxenkg(livraison);
            ecurie.calculPartduFoinTotal();
            ecurie.viderLesBox();
            ecurie.remplirLesBox();
            ecurie.affichageRemplissageDesBox();


        }

    }














