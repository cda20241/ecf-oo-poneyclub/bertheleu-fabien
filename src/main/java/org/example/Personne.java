package org.example;


class Personne {
    private String nom;
    private String prenom;

    //      CONSTRUCTEUR    //
    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
    //      GETTER      //
    public String getNom() {
        return nom;
    }
    //      SETTER      //
    public String getPrenom() {
        return prenom;
    }
}