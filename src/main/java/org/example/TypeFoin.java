package org.example;

public enum TypeFoin {
    PRAIRIE("Foin de Prairie"),
    GRAMINE("Foin de Graminés");

    private String typeDeFoin;

    TypeFoin(String typeDeFoin) {
        this.typeDeFoin = typeDeFoin;
    }
}
