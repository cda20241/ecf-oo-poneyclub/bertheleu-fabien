package org.example;

import java.util.ArrayList;
import java.util.List;

public class Box {
    private Integer taille;
    private Integer quantiteFoinCourante;
    private Integer quantiteFoinMax;
    private List<String>poneyDuBox=new ArrayList<>();
    private String nomDuBox;

    //      CONSTRUCTEUR        //

    public Box(String nomDuBox, Integer taille, Integer quantiteFoinCourante, Integer quantiteFoinMax) {
        this.taille = taille;
        this.quantiteFoinCourante = quantiteFoinCourante;
        this.quantiteFoinMax = quantiteFoinMax;
        this.nomDuBox = nomDuBox;
    }

    public Box(Integer taille, Integer quantiteFoinCourante, Integer quantiteFoinMax) {
        this.taille = taille;
        this.quantiteFoinCourante = quantiteFoinCourante;
        this.quantiteFoinMax = quantiteFoinMax;
    }

    //      GETTER      //

    public Integer getTaille() {
        return taille;
    }

    public Integer getQuantiteFoinCourante() {
        return quantiteFoinCourante;
    }

    public Integer getQuantiteFoinMax() {
        return quantiteFoinMax;
    }

    public List<String> getPoneyDuBox() {
        return poneyDuBox;
    }

    public String getNomDuBox() {
        return nomDuBox;
    }
    //      SETTER      //

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public void setQuantiteFoinCourante(Integer quantiteFoinCourante) {
        this.quantiteFoinCourante = quantiteFoinCourante;
    }

    public void setQuantiteFoinMax(Integer quantiteFoinMax) {
        this.quantiteFoinMax = quantiteFoinMax;
    }

    public void setPoneyDuBox(List<String> poneyDuBox) {
        this.poneyDuBox = poneyDuBox;
    }
    public void setNomDuBox(String nomDuBox) {
        this.nomDuBox = nomDuBox;}

    //      METHODES        //
    public void ajouterPoneyAuBox(Poney poney) {
        poneyDuBox.add(String.valueOf(poney));
    }

    // Supprimer un poney du box
    public void supprimerPoneyDuBox(Poney poney) {
        poneyDuBox.remove(String.valueOf(poney));
    }
}

