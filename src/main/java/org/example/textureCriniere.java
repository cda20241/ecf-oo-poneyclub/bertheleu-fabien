package org.example;

public enum textureCriniere {
    FRISEE("Frisée"),
    LISSE("Lisse");


    private final String textureCriniere;

    textureCriniere(String textureDeCriniere) {
        this.textureCriniere = textureDeCriniere;
    }

}
