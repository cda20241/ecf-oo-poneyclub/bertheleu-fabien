package org.example;

public class Foin {
    private String numeroLot;
    private Integer quantiteLot;
    private TypeFoin typeFoin;
    //      CONSTRUCTEUR        //
    public Foin(String numeroLot, Integer quantiteLot, TypeFoin typeFoin) {
        this.numeroLot = numeroLot;
        this.quantiteLot = quantiteLot;
        this.typeFoin = typeFoin;
    }
    //      GETTER      //

    public String getNumeroLot() {
        return numeroLot;
    }

    public Integer getQuantiteLot() {
        return quantiteLot;
    }

    public TypeFoin getTypeFoin() {
        return typeFoin;
    }

    //      SETTER      //

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public void setQuantiteLot(Integer quantiteLot) {
        this.quantiteLot = quantiteLot;
    }

    public void setTypeFoin(TypeFoin typeFoin) {
        this.typeFoin = typeFoin;
    }
}
