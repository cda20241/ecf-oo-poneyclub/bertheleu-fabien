package org.example;

import java.util.Date;
import java.util.List;

public class Livraison {
    Date date;
    String salarie;
    Foin lotDeFoin; // Lot associé à la livraison

    //      CONSTRUCTEUR        //
    public Livraison(String numeroLot, Integer quantiteLot, TypeFoin typeFoin) {
        this.lotDeFoin = new Foin(numeroLot, quantiteLot, typeFoin);
    }
    ////        GETTER      ////

    public Date getDate() {
        return date;
    }

    public String getSalarie() {
        return salarie;
    }

    public Foin getLotDeFoin() {
        return lotDeFoin;
    }


    ////        SETTER         ////


    public void setDate(Date date) {
        this.date = date;
    }

    public void setSalarie(String salarie) {
        this.salarie = salarie;
    }

    public void setLotDeFoin(Foin lotDeFoin) {
        this.lotDeFoin = lotDeFoin;
    }

    /**Cette méthode pour réceptionner le foin  par un salarié en paramètre et y attribuer la date de réception.
     *
     * @param salarie
     */
      public void receptionnerFoin(Salarie salarie) {
        // Vérification si le foin n'a pas déjà été réceptionné
        if (this.date == null) {
            this.date = new Date(); // on atttribue la date du moment présent
            this.salarie = salarie.getNom() + " " + salarie.getPrenom();
        } else {
            System.out.println("Le foin a déjà été réceptionné.");
        }
    }

    /** je créé une méthode statique pour obtenir la date de livraison d'un lot qui doit rendre null si pas trouvé
     *
     */

    public static Date dateLivraisonDuLot(List<Livraison> livraisons, String numeroLot) {
        for (Livraison livraison : livraisons) {
            if (livraison.lotDeFoin.getNumeroLot().equals(numeroLot)) {
                return livraison.date;
            }
        }
        return null;
    }

}